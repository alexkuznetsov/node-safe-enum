'use strict';

const Proxy = require('node-proxy'),
      Enum = require('enum');

const handlerMaker = (obj) => {
    return {
        get: (receiver, name) => {
            if(obj.hasOwnProperty(name) || ['inspect', 'valueOf'].indexOf(name) !== -1) {
                return obj[name];
            } else {
                throw new Error(`Unknown Enum-Value requested: ${name}`);
            }
        },
        set: (receiver, name, val) => {
            return false;
        } // bad behavior when set fails in non-strict mode
  };
}

module.exports = (enumValues) => {
    return Proxy.create(handlerMaker(new Enum(enumValues)));
}